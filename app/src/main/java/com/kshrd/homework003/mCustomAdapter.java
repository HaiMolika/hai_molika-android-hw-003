package com.kshrd.homework003;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class mCustomAdapter extends BaseAdapter {

    private Context context;
    private List<DataSet> items;
    private LayoutInflater inflater;

    public mCustomAdapter(Context context, List<DataSet> items){
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public DataSet getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.my_content, parent, false);

        TextView title = convertView.findViewById(R.id.title);
        TextView description = convertView.findViewById(R.id.description);

        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setMaxLines(1);

        description.setEllipsize(TextUtils.TruncateAt.END);
        description.setMaxLines(2);

        title.setText(getItem(position).getTitle());
        description.setText(getItem(position).getDescription());

        return convertView;
    }
}
