package com.kshrd.homework003;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

public class AnimationActivity extends AppCompatActivity {

    private ImageView image;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);
        image = findViewById(R.id.image);

        bundle = getIntent().getExtras();
        Integer resource = bundle.getInt("animation");
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),resource);
        image.startAnimation(animation);
    }
}