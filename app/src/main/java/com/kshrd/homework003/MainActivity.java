package com.kshrd.homework003;

import androidx.annotation.ContentView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView title, description, nolist;
    private ListView listView;
    private PopupWindow popupWindow;
    private Button save, cancel;
    private List<DataSet> items;
    private mCustomAdapter adapter;
    private Menu myMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);
        nolist = findViewById(R.id.nolist);
        items = new ArrayList<>();
//        items.add(
//                new DataSet(
//                "IOS",
//                "iOS (formerly iPhone OS) is a mobile operating system created and developed by Apple Inc. exclusively for its hardware. It is the operating system that powers many of the company's mobile devices, including the iPhone and iPod Touch; it also powered the iPad until the introduction of iPadOS, a derivative of iOS, in 2019."
//        ));
//        items.add(
//                new DataSet(
//                        "ANDROID",
//                        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device launched in September 2008."
//                        ));
//        items.add(
//                new DataSet(
//                        "ANDROID",
//                        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device launched in September 2008."
//                        ));
//        items.add(
//                new DataSet(
//                        "IOS",
//                        "iOS (formerly iPhone OS) is a mobile operating system created and developed by Apple Inc. exclusively for its hardware. It is the operating system that powers many of the company's mobile devices, including the iPhone and iPod Touch; it also powered the iPad until the introduction of iPadOS, a derivative of iOS, in 2019."
//                        ));
//        items.add(
//                new DataSet(
//                        "ANDROID",
//                        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device launched in September 2008."
//                        ));
//        items.add(
//                new DataSet(
//                        "ANDROID",
//                        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device launched in September 2008."
//                        ));
//        items.add(
//                new DataSet(
//                        "IOS",
//                        "iOS (formerly iPhone OS) is a mobile operating system created and developed by Apple Inc. exclusively for its hardware. It is the operating system that powers many of the company's mobile devices, including the iPhone and iPod Touch; it also powered the iPad until the introduction of iPadOS, a derivative of iOS, in 2019."
//                ));
//        items.add(
//                new DataSet(
//                        "ANDROID",
//                        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device launched in September 2008."
//                ));
//        items.add(
//                new DataSet(
//                        "ANDROID",
//                        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device launched in September 2008."
//                ));
//        items.add(
//                new DataSet(
//                        "IOS",
//                        "iOS (formerly iPhone OS) is a mobile operating system created and developed by Apple Inc. exclusively for its hardware. It is the operating system that powers many of the company's mobile devices, including the iPhone and iPod Touch; it also powered the iPad until the introduction of iPadOS, a derivative of iOS, in 2019."
//                ));
//        items.add(
//                new DataSet(
//                        "ANDROID",
//                        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device launched in September 2008."
//                ));
//        items.add(
//                new DataSet(
//                        "ANDROID",
//                        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device launched in September 2008."
//                ));
//        nolist.setVisibility(View.GONE);
        adapter = new mCustomAdapter(this, items);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.add:
                initPopupWindow();
                break;
            case R.id.remove:
                items.clear();
                adapter.notifyDataSetChanged();
                nolist.setVisibility(View.VISIBLE);
                break;
            case R.id.fadein:
                intent = new Intent(this, AnimationActivity.class);
                intent.putExtra("animation", R.anim.fadein);
                startActivity(intent);
                break;
            case R.id.fadeout:
                intent = new Intent(this, AnimationActivity.class);
                intent.putExtra("animation", R.anim.fadeout);
                startActivity(intent);
                break;
            case R.id.zoom:
                intent = new Intent(this, AnimationActivity.class);
                intent.putExtra("animation", R.anim.zoom);
                startActivity(intent);
                break;
            case R.id.rotate:
                intent = new Intent(this, AnimationActivity.class);
                intent.putExtra("animation", R.anim.rotate);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void initPopupWindow(){
        View contentView = LayoutInflater.from(this).inflate(R.layout.popup_window, null, false);
        View rootview = LayoutInflater.from(MainActivity.this).inflate(R.layout.activity_main, null);
        popupWindow = new PopupWindow(contentView, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setAnimationStyle(R.style.Animation);
        popupWindow.showAtLocation(rootview, Gravity.CENTER, 0, 0);
        getSupportActionBar().hide();
        popupWindow.update();
        save = contentView.findViewById(R.id.btnsave);
        cancel = contentView.findViewById(R.id.btncancel);
        title = contentView.findViewById(R.id.inputtitle);
        description = contentView.findViewById(R.id.inputdescription);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(title.getText().toString().length() !=0 && description.getText().toString().length() !=0){
                    items.add(new DataSet(title.getText().toString(), description.getText().toString()));
                    adapter.notifyDataSetChanged();
                    nolist.setVisibility(View.GONE);
                }
                popupWindow.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                getSupportActionBar().show();
            }
        });
    }
}